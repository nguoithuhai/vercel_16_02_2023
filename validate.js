// hàm show thông báo
function showmessage(idtag, message) {
  document.getElementById(idtag).innerHTML = message;
}
// hơp lệ ==>true
// kiểm tra mã sinh viên có trùng hay không
function kiemtratrung(id, dssv) {
  let vitri = dssv.findIndex(function (sv) {
    return sv.ma == id;
  });
  if (vitri != -1) {
    // tim thấy
    showmessage("spanMaSV", "mã sinh viên đã tồn tại");
    return false;
  } else {
    showmessage("spanMaSV", " ");
    return true;
  }
}
//kiểm tra email
function kiemtraemail(email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var ismemail = re.test(email);
  if (ismemail) {
    showmessage("spanEmailSV", " ");
    return true;
  } else {
    showmessage("spanEmailSV", "email không hợp lệ ");
    return false;
  }
}
// kiểm tra độ dài
function kiemtradodai(value, iderr, min, max) {
  var length = value.length;
  if (length >= min && length <= max) {
    showmessage(iderr, " ");
    return true;
  } else {
    showmessage(iderr, `trường này phải gồm ${min} đến ${max} kí tự `);
    return false;
  }
}
// kiểm tra tên user
function kiemtrachuoi(value, iderr) {
  var re = /^[a-zA-Z\-]+$/;
  if (re.test(value)) {
    showmessage(iderr, "");
    return true;
  } else {
    showmessage(iderr, "Trường này chỉ gồm chuỗi");
    return false;
  }
}
